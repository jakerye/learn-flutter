# Learn Flutter

See the [Wiki](https://gitlab.com/jakerye/learn-flutter/-/wikis/home)

Left off here: 
- https://codelabs.developers.google.com/codelabs/first-flutter-app-pt2/#8

Flutter Recap
- Written in dart
- Packages at pub.dev
- Starts execution at main()
- Pretty much everything is a widget
- There are stateful and stateless widgets
- Widgets have build methods that "renders" the "component"
- Material design elements (layout, icons, etc.) built into lang
